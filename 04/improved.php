<?php

use AntonSiardziuk\TestTask04\Application\UserList\UserListController;
use AntonSiardziuk\TestTask04\Infrastructure\PDOUserRepository;

require_once __DIR__ .'/vendor/autoload.php';

$pdo = new \PDO('mysql:dbname=test;host=127.0.0.1', 'root', 'root');
$repository = new PDOUserRepository($pdo);
$controller = new UserListController($repository);

$controller->run();
