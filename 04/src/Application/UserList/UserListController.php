<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Application\UserList;


use AntonSiardziuk\TestTask04\Domain\UserRepository;

class UserListController
{
    private $userRepository;
    private $view;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->view = new UserListPageView();
    }

    public function run()
    {
        if (!isset($_GET['user_ids'])) {
            $ids = [];
        } else {
            $ids = explode(',', $_GET['user_ids']);
        }
        $users = $this->userRepository->getByIds($ids);
        $userViews = [];

        foreach ($users as $user) {
            $view = new UserListUserView();
            $user->buildView($view);
            $userViews[] = $view;
        }

        $this->view->render($userViews);
    }
}