<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Application\UserList;


class UserListPageView
{
    /** @param UserListUserView[] $users */
    public function render(array $users)
    {
?>
        <?php foreach ($users as $user): ?>
            <a href="/show_user.php?id=<?=$user->id?>"><?=$user->name?></a>
        <?php endforeach; ?>
<?php
    }
}