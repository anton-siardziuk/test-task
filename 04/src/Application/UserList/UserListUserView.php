<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Application\UserList;


use AntonSiardziuk\TestTask04\Domain\UserView;

class UserListUserView implements UserView
{
    /** @var string */
    public $id;
    /** @var string */
    public $name;

    public function setId(int $id)
    {
        $this->id = (string) $id;
    }

    public function setName(string $name)
    {
        $this->name = htmlentities($name);
    }
}