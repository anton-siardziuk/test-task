<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Domain;

class User
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;

    public static function fromDatabase(int $id, string $name): self
    {
        $user = new self();
        $user->id = $id;
        $user->name = $name;

        return $user;
    }

    public function buildView(UserView $view)
    {
        $view->setId($this->id);
        $view->setName($this->name);
    }
}