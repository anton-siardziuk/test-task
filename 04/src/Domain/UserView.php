<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Domain;


interface UserView
{
    public function setId(int $id);

    public function setName(string $name);
}