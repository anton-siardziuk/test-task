<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Domain;


interface UserRepository
{
    /**
     * @return User[]
     * @throws UserNotFoundException
     */
    public function getByIds(array $ids): array;
}