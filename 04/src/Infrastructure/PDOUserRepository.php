<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask04\Infrastructure;


use AntonSiardziuk\TestTask04\Domain\User;
use AntonSiardziuk\TestTask04\Domain\UserNotFoundException;
use AntonSiardziuk\TestTask04\Domain\UserRepository;

class PDOUserRepository implements UserRepository
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getByIds(array $ids): array
    {
        if (count($ids) === 0) {
            return [];
        }

        $users = [];

        foreach ($ids as $id) {
            $st = $this->pdo->prepare('SELECT * FROM users WHERE id = :id');
            $st->bindValue(':id', $id);
            $st->execute();
            $row = $st->fetch();
            if ($row === false) {
                throw new UserNotFoundException();
            }

            $users[] = User::fromDatabase(
                (int) $row['id'],
                (string) $row['name']
            );
        }

        return $users;
    }
}