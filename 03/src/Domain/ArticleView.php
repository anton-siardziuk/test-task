<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


interface ArticleView
{
    public function setArticleId(int $id): void;

    public function setArticleName(string $name): void;

    public function setArticleAuthor(User $author): void;
}