<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


interface ArticleRepository
{
    public function getById(int $id): Article;

    /** @return Article[] */
    public function getListByAuthor(User $author): ArticleCollection;

    public function save(Article $article): void;
}