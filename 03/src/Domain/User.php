<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


class User
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;

    public function buildView(UserView $view): void
    {
        $view->setUserId($this->id);
        $view->setUserName($this->name);
    }
}