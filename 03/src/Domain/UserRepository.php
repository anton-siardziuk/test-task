<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


interface UserRepository
{
    public function getById(int $id): User;
}