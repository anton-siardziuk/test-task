<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


class Article
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var User */
    private $author;

    public function __construct(string $name, User $author)
    {
        $this->name = $name;
        $this->author = $author;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function changeAuthor(User $newAuthor): void
    {
        $this->author = $newAuthor;
    }

    public function buildView(ArticleView $view): void
    {
        $view->setArticleId($this->id);
        $view->setArticleName($this->name);
        $view->setArticleAuthor($this->author);
    }
}