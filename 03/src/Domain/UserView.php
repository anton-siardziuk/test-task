<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Domain;


interface UserView
{
    public function setUserId(int $id): void;

    public function setUserName(string $name): void;
}