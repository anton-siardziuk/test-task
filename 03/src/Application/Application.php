<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Application;


use AntonSiardziuk\TestTask03\Domain\Article;
use AntonSiardziuk\TestTask03\Domain\ArticleRepository;
use AntonSiardziuk\TestTask03\Domain\UserRepository;

class Application
{
    private $userRepository;
    private $articleRepository;

    public function __construct(UserRepository $userRepository, ArticleRepository $articleRepository)
    {
        $this->userRepository = $userRepository;
        $this->articleRepository = $articleRepository;
    }

    public function createArticle(CreateArticleRequest $request): CreateArticleResponse
    {
        $author = $this->userRepository->getById($request->authorId);

        $article = new Article($request->name, $author);

        $this->articleRepository->save($article);

        $response = new CreateArticleResponse();
        $response->articleId = $article->getId();
    }

    public function showArticle(ShowArticleRequest $request): ShowArticleResponse
    {
        $article = $this->articleRepository->getById($request->articleId);

        $response = new ShowArticleResponse();

        $article->buildView($response);

        return $response;
    }

    public function articleList(ArticleListRequest $request): ArticleListResponse
    {
        $author = $this->userRepository->getById($request->authorId);
        $articles = $this->articleRepository->getListByAuthor($author);

        $response = new ArticleListResponse();
        $response->articles = [];

        foreach ($articles as $article) {
            $articleView = new ArticleListSingleArticleView();
            $article->buildView($articleView);
            $response->articles[] = $articleView;
        }

        return $response;
    }

    public function changeAuthor(ChangeAuthorRequest $request): ChangeAuthorResponse
    {
        $article = $this->articleRepository->getById($request->articleId);
        $newAuthor = $this->userRepository->getById($request->newAuthorId);

        $article->changeAuthor($newAuthor);

        $this->articleRepository->save($article);

        return new ChangeAuthorResponse();
    }
}