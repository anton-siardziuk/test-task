<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Application;


class ChangeAuthorRequest
{
    /** @var int */
    public $articleId;
    /** @var int */
    public $newAuthorId;
}