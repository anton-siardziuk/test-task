<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Application;


class ArticleListResponse
{
    /** @var ArticleListSingleArticleView[] */
    public $articles;
}