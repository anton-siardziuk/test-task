<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Application;


use AntonSiardziuk\TestTask03\Domain\ArticleView;
use AntonSiardziuk\TestTask03\Domain\User;
use AntonSiardziuk\TestTask03\Domain\UserView;

class ArticleListSingleArticleView implements ArticleView, UserView
{
    /** @var int */
    public $authorId;
    /** @var string */
    public $authorName;
    /** @var int */
    public $articleId;
    /** @var string */
    public $articleName;

    public function setArticleId(int $id): void
    {
        $this->articleId = $id;
    }

    public function setArticleName(string $name): void
    {
        $this->articleName = $name;
    }

    public function setArticleAuthor(User $author): void
    {
        $author->buildView($this);
    }

    public function setUserId(int $id): void
    {
        $this->authorId = $id;
    }

    public function setUserName(string $name): void
    {
        $this->authorName = $name;
    }
}