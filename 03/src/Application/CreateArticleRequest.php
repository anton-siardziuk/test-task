<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask03\Application;


class CreateArticleRequest
{
    /** @var string */
    public $name;
    /** @var int */
    public $authorId;
}