<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask02;

use PHPUnit\Framework\TestCase;

class UrlFixerTest extends TestCase
{
    /** @var UrlFixer */
    private $fixer;

    public function setUp(): void
    {
        $this->fixer = new UrlFixer();
    }

    public function testItThrowsExceptionOnEmptyString()
    {
        $this->assertInvalidArgument("");
    }

    public function testShouldAddUrlToRoot()
    {
        $this->assertFix(
            "https://site.com/?url=%2F",
            "https://site.com/"
        );
    }

    public function testShouldAddUrlToAnotherPath()
    {
        $this->assertFix(
            "https://site.com/?url=%2Fsome%2Fpath.html",
            "https://site.com/some/path.html"
        );
    }

    public function testShouldWorkWithHttp()
    {
        $this->assertFix(
            "http://site.com/?url=%2F",
            "http://site.com/"
        );
    }

    public function testShouldWorkWithPort()
    {
        $this->assertFix(
            "http://site.com:8080/?url=%2F",
            "http://site.com:8080/"
        );
    }

    public function testShouldWorkWithAnyDomain()
    {
        $this->assertFix(
            "http://some.other.site.com/?url=%2F",
            "http://some.other.site.com/"
        );
    }

    public function testShouldWorkWithRelativeScheme()
    {
        $this->assertFix(
            "//site.com/?url=%2F",
            "//site.com/"
        );
    }

    public function testWeDoNotAllowSchemeLessUrls()
    {
        $this->assertInvalidArgument("site.com/");
    }

    public function testShouldPassQueryParameter()
    {
        $this->assertFix(
            "//site.com/?param=value&url=%2F",
            "//site.com/?param=value"
        );
    }

    public function testShouldRemoveParameterWithValue3()
    {
        $this->assertFix(
            "//site.com/?param1=1&param2=2&url=%2F",
            "//site.com/?param1=1&param2=2&param3=3"
        );
    }

    public function testShouldOrderParamsByValue()
    {
        $this->assertFix(
            "//site.com/?param2=1&param1=2&url=%2F",
            "//site.com/?param1=2&param2=1"
        );
    }

    private function assertFix(string $fixed, string $toFix)
    {
        $this->assertEquals(
            $fixed,
            $this->fixer->fix($toFix)
        );
    }

    private function assertInvalidArgument(string $url)
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->fixer->fix($url);
    }
}
