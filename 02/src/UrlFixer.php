<?php
declare(strict_types=1);

namespace AntonSiardziuk\TestTask02;


class UrlFixer
{
    public function fix(string $url): string
    {
        if ($url === "") {
            throw new \InvalidArgumentException();
        }

        return $this->scheme($url)
            . $this->host($url)
            . $this->port($url)
            . "/?"
            . $this->query($url);
    }

    private function scheme(string $url): string
    {
        $scheme = parse_url($url, PHP_URL_SCHEME);
        if ($scheme !== null) {
            return $scheme . "://";
        } else {
            return '//';
        }
    }

    private function host(string $url): string
    {
        $host = parse_url($url, PHP_URL_HOST);
        if ($host === null) {
            throw new \InvalidArgumentException();
        }
        return $host;
    }

    private function port(string $url): string
    {
        $port = parse_url($url, PHP_URL_PORT);
        if ($port !== null) {
            return ":" . $port;
        } else {
            return "";
        }
    }

    private function query(string $url): string
    {
        $params = $this->getQueryParamsFromUrl($url);
        $params = $this->removeParamsWithValue3($params);
        $params = $this->sortParamsByValue($params);
        $params = $this->addPathToParams($params, $url);

        return http_build_query($params);
    }

    private function getQueryParamsFromUrl(string $url): array
    {
        $params = [];
        $query = parse_url($url, PHP_URL_QUERY);
        if ($query !== null) {
            parse_str($query, $params);
        }
        return $params;
    }

    private function removeParamsWithValue3(array $params): array
    {
        foreach ($params as $key => $value) {
            if ($value === "3") {
                unset($params[$key]);
            }
        }
        return $params;
    }

    private function sortParamsByValue(array $params): array
    {
        asort($params);

        return $params;
    }

    private function addPathToParams(array $params, string $url): array
    {
        $params['url'] = parse_url($url, PHP_URL_PATH);
        return $params;
    }
}