Новая структура, которую я переделал:

create table users
(
  id         int(11) unsigned auto_increment
    primary key,
  name       varchar(255) null,
  gender     tinyint(1)   null comment 'NULL - не указан, 1 - мужчина, 2 - женщина.',
  birth_date date         null
);

create table phone_numbers
(
  id      int unsigned auto_increment
    primary key,
  user_id int unsigned not null,
  phone   varchar(255) not null,
  constraint FK_phone_numbers_user_id
    foreign key (user_id) references users (id)
);

Самое важное:
- FOREIGN KEY (и автоматом и индекс на phone_numbers.user_id),
- date для дня рождения

остальное более косметические изменения:
- unsigned где надо
- tinyint для gender (ENUM я сознательно не использовал из-за сложностей в изменении значений)
- not null для телефона (смысл тогда создавать запись?)
- null для пола если не указан (более семантично)
- null для даты рождения (потому что можно не указывать имя и пол, значит можно не указывать и дату рождения)

Запрос для выборки пользователей:

SELECT u.id, u.name, COUNT(pn.id)
FROM users u
LEFT JOIN phone_numbers pn on u.id = pn.user_id
WHERE u.birth_date BETWEEN
  DATE_SUB(CURDATE(), INTERVAL 38 YEAR)
  AND
  DATE_SUB(CURDATE(), INTERVAL 28 YEAR)
GROUP BY u.id, u.name;

Почему добавился id: если строго, то в SELECT можно использовать только те поля, которые были в GROUP BY, если
есть агрегирующая функция, а GROUP BY name не очень, потому что а вдруг несколько пользователей с
одинаковыми именами.