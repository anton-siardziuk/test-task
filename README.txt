Вопрос №1

Много лет пишу тесты на behat, очень разные. От UI тестов при помощи селениума до тестов чисто бизнес-логики на behat
даже без базы данных реальной. Собственно, уже не представляю разработку любого проекта без этого.
Unit-тесты местами тоже пишу, на те вещи в основном, которые очень легко покрыть юнит-тестами. Обычный пример класса,
покрытого юнит-тестами - задание #2. Абсолютно так же это бы выглядело у меня в реальном проекте.
Из нагрузочных тестов - использовали немного loadimpact.com. Писали скрипт достаточно простой, который делал определенные
действия (заказывал билет) и потом loadimpact его выполнял параллельно в много потоков.

Вопрос №2

Вот список книг, которые я читал: https://www.goodreads.com/review/list/54193018-anton-serdyuk?order=d&page=1&shelf=read&sort=date_read
К сожалению читаю не так много как хотелось бы
Из наиболее важных и наиболее на меня повлиявших или понравившихся могу отметить эти:
* Designing Data-Intensive Applications: The Big Ideas Behind Reliable, Scalable, and Maintainable Systems
* Reinventing Organizations: A Guide to Creating Organizations Inspired by the Next Stage of Human Consciousness
* Specification by Example
* Bridging the Communication Gap: Specification by Example and Agile Acceptance Testing
* Domain-Driven Design: Tackling Complexity in the Heart of Software
* The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses
* Continuous Delivery: Reliable Software Releases Through Build, Test, and Deployment Automation